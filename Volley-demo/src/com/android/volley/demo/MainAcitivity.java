package com.android.volley.demo;

import android.app.Activity;
import android.os.Bundle;

import com.android.volley.manager.VolleyManager;
import com.android.volley.toolbox.NetworkImageView;

public class MainAcitivity extends Activity {

	private NetworkImageView mVolleyIv;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acitvity_main);
		
		initView();

	}

	private void initView() {
		mVolleyIv = (NetworkImageView) findViewById(R.id.iv);
		mVolleyIv.setDefaultImageResId(R.drawable.ic_launcher);
		mVolleyIv.setErrorImageResId(R.drawable.ic_launcher);
		mVolleyIv.setImageUrl("http://pic4.nipic.com/20091218/3557379_083034065299_2.jpg",
				VolleyManager.getInstance(this).getImageLoader());
		
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

}
