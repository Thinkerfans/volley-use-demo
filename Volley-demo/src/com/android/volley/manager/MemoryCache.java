package com.android.volley.manager;

import java.lang.ref.SoftReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;

public class MemoryCache {
	
	@SuppressLint("UseSparseArrays")
	private static Map<String, SoftReference<Bitmap>> cache = Collections.synchronizedMap(new HashMap<String, SoftReference<Bitmap>>());

	public static Bitmap get(String id){
		if(!cache.containsKey(id))
			return null;
		SoftReference<Bitmap> ref = cache.get(id);
		return ref.get();
	}

	public static void put(String id, Bitmap bitmap){
		cache.put(id, new SoftReference<Bitmap>(bitmap));
	}

	public static void clear() {
		cache.clear();
	}
}