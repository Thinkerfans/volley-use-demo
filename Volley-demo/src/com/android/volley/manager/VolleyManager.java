package com.android.volley.manager;

import android.content.Context;
import android.graphics.Bitmap;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageCache;
import com.android.volley.toolbox.Volley;

public class VolleyManager {

	private static VolleyManager sInstance;
	private RequestQueue mQueue;
	private ImageLoader mLoader;

	private VolleyManager(Context c) {
		mQueue = Volley.newRequestQueue(c);
		mLoader = new ImageLoader(mQueue, new ImageCache() {
			@Override
			public void putBitmap(String url, Bitmap bitmap) {
				MemoryCache.put(url, bitmap);
			}

			@Override
			public Bitmap getBitmap(String url) {
				return MemoryCache.get(url);
			}
		});
	}

	public static synchronized VolleyManager getInstance(Context c) {
		if (sInstance == null) {
			sInstance = new VolleyManager(c);
		}
		return sInstance;
	}

	public ImageLoader getImageLoader() {
		return mLoader;
	}

}
